/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entitites.RatingSeller;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author dle
 */
@Stateless
public class RatingSellerFacade extends AbstractFacade<RatingSeller> {

    @PersistenceContext(unitName = "lokal")
    private EntityManager em;

    //Requirement: Rate seller with comment
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RatingSellerFacade() {
        super(RatingSeller.class);
    }
    
}
