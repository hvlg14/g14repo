/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entitites.AuctionItem;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author dle
 */
@Stateless
public class AuctionItemFacade extends AbstractFacade<AuctionItem> {

    @PersistenceContext(unitName = "lokal")
    private EntityManager em;

    //Requirement: find a given auctionItem (id) - use abstract method
    
    //Requirement: (AuctionItem) Poll(Trigger) for items that have reached the timelimit, and set them to not published and sold or not
    
    
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuctionItemFacade() {
        super(AuctionItem.class);
    }
    
}
