/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entitites.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author dle
 */
@Stateless
public class ProducteFacade extends AbstractFacade<Product> {

    @PersistenceContext(unitName = "lokal")
    private EntityManager em;

    //Requirement: Search for product given category
    
    
    //Requirement: Search for product given free text search
    public List findProductWithFreeTextString(String searchText) {
        String query = "SELECT p FROM Product p WHERE p.name LIKE '%" + searchText + "%'" + " OR p.category LIKE '%" + searchText + "%'";
        System.out.println(query);
        return em.createQuery(query).setMaxResults(20).getResultList();
    }
    
    //Requirement: List products, only the published ones, with the oldest product on top
    
    
    
    // EXAMPLECODE (not tested): Retrieves all the products given a sellerID:
    public List<Product> findProductsGivenSeller(long sellerID) {
        TypedQuery<Product> query = em.createQuery(
            "SELECT p FROM Product p ORDER BY p.id where sellerid=" + sellerID, Product.class);
        return query.getResultList();
    }
    
    // EXAMPLECODE (not tested): Retrieves all the products given a category:
    public List<Product> findProductsGivenCategory(String category) {
        CriteriaBuilder cbuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Product> cquery = cbuilder.createQuery(Product.class);
        Root<Product> root = cquery.from(Product.class);
        cquery.where(cbuilder.equal(root.get("category"), category));
        return getEntityManager().createQuery(cquery).getResultList();
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProducteFacade() {
        super(Product.class);
    }
    
}
