/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entitites.FeedBackProduct;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author dle
 */
@Stateless
public class FeedBackProductFacade extends AbstractFacade<FeedBackProduct> {

    @PersistenceContext(unitName = "lokal")
    private EntityManager em;

    //Requirement: Search for product given category
    
    
    //Requirement: Search for product given free text search
    
    
    //Requirement: List products, only the published ones, with the oldest product on top
    
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FeedBackProductFacade() {
        super(FeedBackProduct.class);
    }
    
}
