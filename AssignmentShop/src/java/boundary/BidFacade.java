/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entitites.Bid;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author dle
 */
@Stateless
public class BidFacade extends AbstractFacade<Bid> {

    @PersistenceContext(unitName = "lokal")
    private EntityManager em;

    //Requirement: (BID) Add a validated bid (itemID, customerID, sum, maxsum, autosum, interval)
        //Access the auctionItem (itemID) with @EJB
            //validate the item is not sold
            //validate the item is published
    
        //Access the bid
            //validate the sum is higher then the current bid(selct max from bid where auctionItem_id = itemID)
            //validate the bid-time is ok
            //add the current bid (with possible autobid and interval)
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BidFacade() {
        super(Bid.class);
    }
    
}
