/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ProducteFacade;
import entitites.Product;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author dle and others
 */
@Named(value = "ProductView")
@RequestScoped
public class ProductView {

    @EJB
    private ProducteFacade productFacade;

    // Creates a new field
    private Product product;

    private String searchString;
    
    // Calls getMessage to retrieve the message
    public Product getProduct() {
       return product;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
    
    
    // Returns the total number of messages
    public int getNumberOfProducts(){
       return productFacade.findAll().size();
    }

   // Returns the total number of messages
    public List<Product> getAllProducts(){
       return productFacade.findAll();
    }
    
    
    
    // Returns the product given the freeTextSearch
    public List<Product> getAllFreeTextProducts(){
       return productFacade.findProductWithFreeTextString(this.searchString);
    }
   
    // Searches for the product
    public String searchProduct(){
       return "listsearchedproducts";
    }

    
    // Saves the product 
    public String postProduct(){
       this.productFacade.create(product);
       return "listproducts";
    }

  
    /** Creates a new instance of ProductView */
    public ProductView() {
       this.product = new Product();
    }
    
}
